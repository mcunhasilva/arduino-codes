#include <avr/io.h>
#include <util/delay.h>
#include "read.h"

int main(void)
{
	/* I/O cfg */
	DDRB = (1 << DDB5);

	for (;;) {
		if(bit_is_set(PINB,PINB4)){
			PORTB |= (1<<PORTB5);
		} else{ 
			PORTB &= ~(1<<PORTB5);
		}	
	}
	main_loop();
}